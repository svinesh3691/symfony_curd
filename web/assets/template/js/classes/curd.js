// Adding a jQuery plugin used to fill the form fields- TESTING PURPOSE
$.fn.fillUp = function(fills){
      var inputs = this.find("input[type!='hidden']");
      inputs.each(function(k){
            $(inputs[k]).val(fills[k]);
      }); 
      $("button[type='submit']").click();
}

// Class CURD : To manage the complete action of a curd module
var curd = function(module, module_name, tableOptions) {
      this.module = module; 
      this.module_name = module_name; 
      this.tableOptions = tableOptions; 
      this.init();
}

/*
* Method Name   getAddForm
* 
* @purpose      populate the modal box with add form
* @parameters   null
* @return       null
*/
curd.prototype.getAddForm = function() {
      var _this = this;
      $.get(this.module+'_add_form',{},function(response){
            $('.'+_this.module+'ModelBody').html(response);
            _this.addFormSubmitManagement();
      });
}

/*
* Method Name   getEditForm
* 
* @purpose      populate the modal box with edit form
* @parameters   null
* @return       null
*/
curd.prototype.getEditForm = function(id) {
      var _this = this;
      $.get(this.module+'_edit_form/'+id,{},function(response){
            $('.'+_this.module+'ModelBody').html(response);
            _this.editFormSubmitManagement(id);
      });
}

/*
* Method Name   addFormSubmitManagement
* 
* @purpose      for submit management of add form
* @parameters   null
* @return       null
*/
curd.prototype.addFormSubmitManagement = function() {
      var _this = this;
      var form = $("form[name='form']");
      form.submit(function(e){
            e.preventDefault();
            var formSerialize = $(this).serialize();
            $.post(_this.module+'_add_form', formSerialize, function(response){
                  if(response.status == 'success') {
                        $('#'+_this.module+'Model').modal('hide');
                        new PNotify({
                            title: 'Success',
                            text: _this.module_name+' added successfully',
                            type: 'success',
                            styling: 'bootstrap3'
                        });
                        _this.dataTable.ajax.reload(null,false); 
                  }
            },'JSON');
      });
      form.validate();

}

/*
* Method Name   editFormSubmitManagement
* 
* @purpose      for submit management of edit form
* @parameters   id -> id of the entry to be editted
* @return       null
*/
curd.prototype.editFormSubmitManagement = function(id) {
    var _this = this;
    var form = $("form[name='form']");
    form.submit(function(e){
        e.preventDefault();
        var formSerialize = $(this).serialize();
        $.post(_this.module+'_edit_form/'+id, formSerialize, function(response){
            if(response.status == 'success') {
                  $('#'+_this.module+'Model').modal('hide');
                  new PNotify({
                      title: 'Success',
                      text: _this.module_name+' updated successfully',
                      type: 'success',
                      styling: 'bootstrap3'
                  });
                  _this.dataTable.ajax.reload(null,false); 

            }
        },'JSON');
    });

    form.validate();

}


/*
* Method Name   deleteEntry
* 
* @purpose      for deleting an entry
* @parameters   id -> id of the entry to be deleted
* @return       null
*/
curd.prototype.deleteEntry = function(id) {
      var _this = this;
      $.post(_this.module+'_delete', {'id':id}, function(response){
            if(response.status == 'success') {
                  _this.dataTable.ajax.reload(null,false); 
                  new PNotify({
                      title: 'Success',
                      text: _this.module_name+' deleted successfully',
                      type: 'success',
                      styling: 'bootstrap3'
                  });
            }
      },'JSON');
}

/*
* Method Name   init
* 
* @purpose      For adding eventlisteners of the page and
*               execute some functions during page load
* @parameters   tableOptions -> options for the data table
* @return       null
*/
curd.prototype.init = function (tableOptions) {
    var _this = this;

      _this.dataTable = HaseDatatable.dataTableInit(_this.module+"Listing",_this.tableOptions); 

      /*Add Button Event listener*/
      $('.'+this.module+'AddButton').on('click', function () {
            $('.'+_this.module+'-modal-title').html('Add New '+ _this.module_name);
            $('.'+_this.module+'ModelBody').html('Loading...');
            $('#'+_this.module+'Model').modal('show');
            _this.getAddForm();
      }); 
    

      /*Edit Button Event listener*/
      $(document).on('click','.'+this.module+'EditButton', function () {
            $('.'+_this.module+'-modal-title').html('Edit '+ _this.module_name);
            $('.'+_this.module+'ModelBody').html('Loading...');
            $('#'+_this.module+'Model').modal('show');
            _this.getEditForm($(this).data('id'));
      }); 


      /*Delete Button Event listener*/
      $(document).on('click','.'+this.module+'DeleteButton', function () {
            if(confirm("Are you sure to delete?")) _this.deleteEntry($(this).data('id'));
      }); 
        
}

/*
* Method Name   createEntry
* 
* @purpose      To automatically add values to form
* @parameters   fills -> array of form values
* @return       null
*/
curd.prototype.createEntry = function(fills) {
    var _this = this;
    $("."+_this.module+"AddButton").click();
    setTimeout(function(){
        $('form').fillUp(fills);
    },1000);
    
}   










