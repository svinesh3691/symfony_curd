
var HaseDatatable = function () {
    var settings;
    var instData;
    var listTable;
    var defaultSettings = {
        stateSaveFlag:true,
        serverSideprocess:false,
        ajaxPath:'',
        pagination: true,
        columnDefFlag:true,
        opt: {
            paging: true,
            stateSave: true,
            stateDuration: 60 * 60 * 24,
            responsive: true,
            deferRender: true,
            lengthChange: true,
            pagination: true,
            rowLength: true,
            scrollX: true,
            pagingType: 'full_numbers',
            processing: true,
            serverSide: false
        }

        

    }
    //function to merge the new option value with default option value
    var initSettings = function (options) {
        settings = $.extend(true, {}, defaultSettings, options);

    }

    var initdatatable = function (tableId, options) {
        if (!jQuery().dataTable) {
            return;
        }
        //options merging function
        initSettings(options);
        //set the id of table
        settings.tableId = tableId;
        instData = $('#' + tableId);
        //server side process
        if (settings.serverSideprocess) {
            settings.opt.serverSide = true;
            settings.opt.processing = true;

        } else {
            settings.opt.serverSide = false;
            settings.opt.processing = true;
        }
        if (settings.ajaxPath != '') {
            settings.opt.ajax = {
                "url": settings.ajaxPath,
                "type": "POST",
                "dataSrc": function (json) {
                   return json.aaData;
                }
            };
        } 
        if (settings.columnDefFlag) {
            settings.opt.columnDefs = settings.columnDefValues;
        }       
        if(!settings.stateSaveFlag) {
           settings.opt.stateSave=false; 
        }
        listTable = $('#' + tableId).DataTable(settings.opt);
       //refresh the datatable while resize a page 
        $(window).resize(function () {
           listTable.ajax.reload();
        });
       
        return listTable;

    }
   
    
    
    return {
        //Function to use the datatable init
        dataTableInit: function (tableId, options) {
            var listTableObj = initdatatable(tableId, options);
            return listTableObj;
        }
        
    };

}();

