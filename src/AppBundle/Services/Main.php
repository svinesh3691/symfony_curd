<?php
namespace AppBundle\Services;

class Main
{
    /**
     * Get all errors of a given form.
     *
     * @param  $form
     *
     * @return array
     */
    public function getFormErrors($form)
    {
        $errors = array();

        // Global
        foreach ($form->getErrors() as $error) {
            $errors[$form->getName()][] = $error->getMessage();
        }

        // Fields
        foreach ($form as $child) {
            if (!$child->isValid()) {
                foreach ($child->getErrors() as $error) {
                    $errors[$child->getName()][] = $error->getMessage();
                }
            }
        }

        return $errors;
    }
    
}
