<?php

namespace AppBundle\Repository;

use Doctrine\ORM\EntityRepository;

class UserRepository extends EntityRepository
{
	
	/**
     * @param string $role
     *
     * @return array
     */
    public function getUserByRoles($role)
    {

        $users = $this->createQueryBuilder('u')
                        ->select('u.id', 'u.username', 'u.email', 'u.firstName', 'u.lastName')
                        ->where('u.roles LIKE :role')
		                ->setParameter('role', '%'.$role.'%')
		                ->getQuery()
		                ->getArrayResult();

        return $users;
    }



}