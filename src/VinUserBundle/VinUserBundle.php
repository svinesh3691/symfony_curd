<?php

namespace VinUserBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class VinUserBundle extends Bundle
{
    public function getParent()
    {
        return 'FOSUserBundle';
    }
}