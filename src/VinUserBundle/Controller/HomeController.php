<?php

namespace VinUserBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class HomeController extends Controller
{

    public function indexAction(Request $request)
    {
    	$logged_user = $this->getUser();
        return $this->render('VinUserBundle::home.html.twig', [
            'username' => $logged_user->getFirstName() .' '. $logged_user->getLastName(),
            'userrole'  => $logged_user->getRoles(),
            'base_dir' => realpath($this->getParameter('kernel.root_dir').'/..'),
        ]);
    }
}
