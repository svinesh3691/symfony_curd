<?php

namespace VinUserBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use AppBundle\Entity\User;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\ResetType;

class EmployeeController extends Controller
{

    /**
     * Method to the main page of employee management
     *
     * @param    $request       the request object
     *
     * @return   retuns a html page
     *
     */
    public function indexAction(Request $request)
    {
        // Checking privillage
        if(!$this->checkPrivilage()) {
            return false;
        }
       
        $logged_user = $this->getUser();
        $users = $this->getDoctrine()->getRepository('AppBundle:User')->findAll();

        return $this->render('VinUserBundle:Employee:main.html.twig', [
            'users'     => $users,
            'username'  => $logged_user->getFirstName() .' '. $logged_user->getLastName(),
            'userrole'  => $logged_user->getRoles(),
            'base_dir'  => realpath($this->getParameter('kernel.root_dir').'/..'),
        ]);
    }

    /**
     * Method to get the list of employees
     *
     * @param    $request       the request object
     *
     * @return   retuns a json response of employe list
     *
     */
    public function jsonListAction(Request $request)
    {
        $users = $this->getDoctrine()->getRepository('AppBundle:User')->getUserByRoles('ROLE_EMPLOYEE');
        return new Response(json_encode($users));
    }

    /**
     * Method to create employee
     *
     * @param    $request       the request object
     *
     * @return   retuns a json reponse or the complete form based on the request
     *
     */
    public function addFormAction(Request $request)
    {
        // Checking privillage
        if(!$this->checkPrivilage()) {
            return false;
        }

        $entityManager = $this->get('doctrine')->getManager();

        // creating a new user object
        $user = new User();
        // creating the form
        $form = $this->createFormBuilder($user)
                        ->add('firstName',TextType::class,['attr' => ['class' => 'form-control']])
                        ->add('lastName',TextType::class,['attr' => ['class' => 'form-control']])
                        ->add('email',EmailType::class,['attr' => ['class' => 'form-control']])
                        ->add('plainPassword',TextType::class,['attr' => ['class' => 'form-control']])
                        ->getForm();
        $form->handleRequest($request);

        // When we get the post request
        if ($request->getMethod() == 'POST') {
            
            // If the form is submitted
            if ($form->isSubmitted()) {

                // setting some defautts data for employee
                $user->setEnabled(1);
                $user->setUsername($form['email']->getData());
                $user->setRoles(['ROLE_EMPLOYEE']);
                
                // Checking for validation errors
                $errors     = $this->get('validator')->validate($user);
                if (count($errors) > 0) { // If validation error exists
                    $errors_array = $this->get('hase_main')->getFormErrors($form);
                    $response = [
                            'status'        => 'failed',
                            'error_type'    => 'validation_error',
                            'errors'        => $errors_array, 
                    ];

                    return new Response(json_encode($response));

                } else { // If no validation error

                    $entityManager->persist($user);
                    $entityManager->flush();
                    $response = ['status' => 'success'];

                    return new Response(json_encode($response));
                }
            }
        }
        
        // If the request is a get request respond with the employee add form
        return $this->render('VinUserBundle:Employee:form.html.twig', [
            'form' => $form->createView(),
            'base_dir' => realpath($this->getParameter('kernel.root_dir').'/..'),
        ]);
    }

    /**
     * Method to edit employee
     *
     * @param    $request       the request object
     *
     * @return   retuns a json reponse or the complete form based on the request
     *
     */
    public function editFormAction(Request $request)
    {
        // Checking privillage
        if(!$this->checkPrivilage()) {
            return false;
        }

        $entityManager = $this->get('doctrine')->getManager();

        // Getting the employee id from the request payload
        $employee_id = $request->attributes->get('employee_id');
        // Getting the user object to edit using the doctrine
        $user =   $this->getDoctrine()->getRepository('AppBundle:User')->findOneById($employee_id);
        
        // Creating the form
        $form = $this->createFormBuilder($user)
                        ->add('firstName',TextType::class,['attr' => ['class' => 'form-control']])
                        ->add('lastName',TextType::class,['attr' => ['class' => 'form-control']])
                        ->add('email',EmailType::class,['attr' => ['class' => 'form-control']])
                        ->getForm();
        
        $form->handleRequest($request);

        // If the request is a post request 
        if ($request->getMethod() == 'POST') {
            if ($form->isSubmitted() && $form->isValid()) {
                $user->setEnabled(1);
                $user->setUsername($form['email']->getData());
                $user->setRoles(['ROLE_EMPLOYEE']);
                $entityManager->persist($user);
                $entityManager->flush();
               return new Response(json_encode(array('status'=>'success')));
            }
        }
        
        
        return $this->render('VinUserBundle:Employee:form.html.twig', [
            'form' => $form->createView(),
            'base_dir' => realpath($this->getParameter('kernel.root_dir').'/..'),
        ]);
    }

    /**
     * Method to delete employee
     *
     * @param    $request       the request object
     *
     * @return   retuns a json reponse of the delete status
     *
     */
    public function deleteAction(Request $request)
    {
        // Checking privillage
        if(!$this->checkPrivilage()) {
            return false;
        }

        $entityManager = $this->get('doctrine')->getManager();

        $employee_id = $request->request->get('id');
        $user =   $this->getDoctrine()->getRepository('AppBundle:User')->findOneById($employee_id);
        
        $entityManager->remove($user);
        $entityManager->flush();
        return new Response(json_encode(array('status'=>'success')));
    }

     /**
     * Method to check privillege
     *
     * @return   retuns a boolean
     *
     */
    protected function checkPrivilage() {
        $roles = $this->getUser()->getRoles();
        if (in_array('ROLE_SUPER_ADMIN', $roles)) {
            return true;
        } else {
            return false;
        }
    }
}





/* 

Discoveries

To get current user 
$user = $this->getUser();
_________________________________________


*/